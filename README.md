# COMPETENCIA DE LOGO EN PYTHON - INGELECTRA 2020

## Ingelectra 2020

Objetivo: Usando Python escribir con caracteres ASCII un logo que diga "Ingelectra 2020".
> [Bases del concurso](n9.cl/logo-python-ingelectra).

## Instrucciones

Para ejecutar el código en condiciones para la competición solo es necesario clonar el
repositorio y ejecutar:
```shell
$ python3 main.py
```
El script por defecto usa una versión de la fuente ASCII mínima para el concurso
`doom_font_min.py`, pero para otros usos esta `doom_font_complete.py`. Solo es necesario
cambiar el `import` al inicio del `doom_text.py`.

## Datos Personales

Diego Ignacio Muñoz Viveros, <diegoignacio.munoz@alumnos.ulagos.cl>.

5° año de Ingeniería Civil Informática, Universidad de Los Lagos.

## Créditos por la fuente

Fuente extraída de: [Generador de
fuente](http://patorjk.com/software/taag/#p=display&f=Doom&t=A%0AB%0AC%0AD%0AE%0AF%0AG%0AH%0AI%0AJ%0AK%0AL%0AM%0AN%0AO%0AP%0AQ%0AR%0AS%0AT%0AU%0AV%0AW%0AX%0AY%0AZ%0Aa%0Ab%0Ac%0Ad%0Ae%0Af%0Ag%0Ah%0Ai%0Aj%0Ak%0Al%0Am%0An%0Ao%0Ap%0Aq%0Ar%0As%0At%0Au%0Av%0Aw%0Ax%0Ay%0Az%0A0%0A1%0A2%0A3%0A4%0A5%0A6%0A7%0A8%0A9)

> DOOM by Frans P. de Vries <fpv@xymph.iaf.nl>  18 Jun 1996 based on Big by Glenn
> Chappell 4/93 -- based on Standard figlet release 2.1 -- 12 Aug 1994 Permission is
> hereby given to modify this font, as long as the modifier's name is placed on a comment
> line.
