#from doom_font_complete import *
from doom_font_min import *


class Doom_text:

    def __init__(self, text, alphabet=ALPHABET):
        
        self.text = text
        self.alphabet = alphabet


    def __str__(self):
        
        words = self.text.split()
        final_text = ''
        for word in words:
            for letter_level in range(11):
                aux = [self.alphabet[letter].split('\n') for letter in word]
                level = ''.join([letter[letter_level] for letter in aux])
                final_text += level + '\n'
        return final_text
